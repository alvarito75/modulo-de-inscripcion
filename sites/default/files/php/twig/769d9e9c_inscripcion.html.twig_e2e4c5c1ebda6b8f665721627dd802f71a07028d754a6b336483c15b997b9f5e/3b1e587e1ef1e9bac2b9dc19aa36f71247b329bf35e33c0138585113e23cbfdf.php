<?php

/* modules/custom/inscripcion/templates/inscripcion.html.twig */
class __TwigTemplate_bc4c032f7c32939c88d297fb91b9ec5c04cb7b8e9865aa3d9b10e1cd1c9decc7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array(),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 11
        echo "<div id=\"content-form-registration\" ng-controller=\"inscripcion\">
  ";
        // line 13
        echo "  <div class=\"wrapper-personal-registration\">
    <label>I Información personal del estudiante</label>
    <br>
    <div class=\"wrapper-name-lastname\">
      <label>Apellidos (paterno y materno) </label><input  type=\"textfied\" name=\"apellidos\" placeholder=\"\">
      <label>Nombres </label><input  type=\"textfied\" name=\"nombre\" placeholder=\"\">  
    </div>
    <br>
    <div class=\"wrapper-dates\">
      <label>Fecha de nacimiento </label><input  type=\"textfied\" name=\"fechaNac\" placeholder=\"\">
      <label>Lugar de nacimiento </label><input  type=\"textfied\" name=\"lugarNac\" placeholder=\"\">
      <label>Nacionalidad </label><input  type=\"textfied\" name=\"nacionalidad\" placeholder=\"\">
    </div>
    <br>
    <div class=\"wrapper-age\">
      <label>Grado al que aplica </label><input  type=\"textfied\" name=\"curso\" placeholder=\"\">
      <label>Edad al 30 de junio de 2016 </label><input  type=\"number\" name=\"edad\" placeholder=\"\">
    </div>
    <br>
    <div class=\"wrapper-phones\">
      <label>Cuanto tiempo reside en Cbba </label><input  type=\"textfied\" name=\"tiempoRes\" placeholder=\"\">
      <label>Teléfono(s) domicilio </label><input  type=\"textfied\" name=\"telefono\" placeholder=\"\">
      <label>Teléfono(s) celular </label><input  type=\"textfied\" name=\"celular\" placeholder=\"\">
    </div>
    <br>
    <div class=\"wrapper-address\">
      <label>Dirección: Calle/Av. Nro. Zona</label><input type=\"textfield\" name=\"direccion\" placeholder=\"\">
    </div>
    <br>
    <div class=\"wrapper-colleges\">
      <label>Enumere en orden cronológico los colegios en el que el estudiante estuvo anteriormente</label>
      <br>
      <label>1) Nombre <input type=\"textfield\" name=\"primer-colegio\"> Año <input type=\"textfield\" name=\"primer-año\"> Ciudad/País <input type=\"textfield\" name=\"primer-pais\"></label>
      <label>2) Nombre <input type=\"textfield\" name=\"segundo-colegio\"> Año <input type=\"textfield\" name=\"segundo-año\"> Ciudad/País <input type=\"textfield\" name=\"segundo-pais\"></label>
      <label>3) Nombre <input type=\"textfield\" name=\"tercer-colegio\"> Año <input type=\"textfield\" name=\"tercer-año\"> Ciudad/País <input type=\"textfield\" name=\"tercer-pais\"></label>
    </div>
    <br>
    <div class=\"wrapper-others\">
      <label>Dominio del Ingles: <label><input type=\"radio\" name=\"idioma\" value=\"\"> Nada </label><label><input type=\"radio\" name=\"idioma\" value=\"\"> Poco </label><label><input type=\"radio\" name=\"idioma\" value=\"\"> Fluido </label></label>
      <label>Otros idiomas: <input type=\"textfield\" name=\"otros-idiomas\" placeholder=\"Separe los idiomas por comas\"></label>
      <label>Estado de salud del alumno: <input type=\"textfield\" name=\"estado-salud\" placeholder=\"\"></label>
      <label>¿Fue suspendido o expulsado de algún colegio? <label><input type=\"radio\" name=\"suspendido\" value=\"\"> Si </label><label><input type=\"radio\" name=\"suspendido\" value=\"\"> No </label></label>
      <label>Si la respuesta es afirmativa, favor indique el motivo</label>
      <input type=\"textarea\" name=\"motivo\">
    </div>
  </div>
  <br>

  ";
        // line 62
        echo "  <div class=\"wrapper-personal-family\">
    <label>II Información Personal de la Familia</label>
    <br>
    <label>¿Algún miembro se congrega? <label> Si <input type=\"radio\" name=\"suspendido\" value=\"\"></label><label> No <input type=\"radio\" name=\"suspendido\" value=\"\"></label></label>
    <label>Si su respuesta es afirmativa, favor indique la congregación </label>
    <input type=\"textarea\" name=\"motivo\">
    <div class=\"wrapper-father-information\" style=\"width: 50%;\">
      <label>Nombre completo del padre <input type=\"textfield\" name=\"nombre-papa\"></label>
      <label>Teléfono casa / Teléfono oficina <input type=\"textfield\" name=\"telefono-papa\"></label>
      <label>Teléfono Celular / Casilla <input type=\"textfield\" name=\"celular-papa\"></label>
      <label>Dirección correo electrónico <input type=\"textfield\" name=\"correo-papa\"></label>
      <label>Dirección domicilio <input type=\"textfield\" name=\"direccion-papa\"></label>
      <label>Profesión <input type=\"textfield\" name=\"profesion-papa\"></label>
      <label>Nacionalidad <input type=\"textfield\" name=\"nacionalidad-papa\"></label>
    </div>
    <div class=\"wrapper-mother-information\" style=\"width: 50%;\">
      <label>Nombre completo del madre <input type=\"textfield\" name=\"nombre-madre\"></label>
      <label>Teléfono casa / Teléfono oficina <input type=\"textfield\" name=\"telefono-madre\"></label>
      <label>Teléfono Celular / Casilla <input type=\"textfield\" name=\"celular-madre\"></label>
      <label>Dirección correo electrónico <input type=\"textfield\" name=\"correo-madre\"></label>
      <label>Dirección domicilio <input type=\"textfield\" name=\"direccion-madre\"></label>
      <label>Profesión <input type=\"textfield\" name=\"profesion-madre\"></label>
      <label>Nacionalidad <input type=\"textfield\" name=\"nacionalidad-madre\"></label>
    </div>
    <label>Indique los hijos que conforman su familia</label>
    <table>
      <tr><th></th><th>Nombre completo</th><th>Fecha de nacimiento</th><th>Colegio</th><th>Curso</th></tr>
      <tr><td><label>1</label></td><td><input type=\"textfield\" name=\"\"></td><td><input type=\"textfield\" name=\"\"></td><td><input type=\"textfield\" name=\"\"></td><td><input type=\"textfield\" name=\"\"></td></tr>
      <tr><td><label>2</label></td><td><input type=\"textfield\" name=\"\"></td><td><input type=\"textfield\" name=\"\"></td><td><input type=\"textfield\" name=\"\"></td><td><input type=\"textfield\" name=\"\"></td></tr>
      <tr><td><label>3</label></td><td><input type=\"textfield\" name=\"\"></td><td><input type=\"textfield\" name=\"\"></td><td><input type=\"textfield\" name=\"\"></td><td><input type=\"textfield\" name=\"\"></td></tr>
      <tr><td><label>4</label></td><td><input type=\"textfield\" name=\"\"></td><td><input type=\"textfield\" name=\"\"></td><td><input type=\"textfield\" name=\"\"></td><td><input type=\"textfield\" name=\"\"></td></tr>
      <tr><td><label>5</label></td><td><input type=\"textfield\" name=\"\"></td><td><input type=\"textfield\" name=\"\"></td><td><input type=\"textfield\" name=\"\"></td><td><input type=\"textfield\" name=\"\"></td></tr>
    </table>
    <label>Favor indicar si los padres son: <label>Divorciados <input type=\"radio\" name=\"estado-padre\"></label><label>Padre fallecido <input type=\"radio\" name=\"estado-padre\"></label><label>Madre fallecida <input type=\"radio\" name=\"estado-padre\"></label></label>
  </div>
  <br>
  <div class=\"wrapper-contract\">
    <p>Estoy en conocimiento que la aceptación de mi(s) hijos(as) como alumno(a) del colegio Cristo Nación esta sujeta a condiciones estipuladas en el Estatuto Orgánico y Reglamento Interno.</p>

    <p>Entiendo que al momento de ser admitido mi hijo(a), cancelaré la primera cuota y todos los demás conceptos relacionados al costo de la colegiatura mensual y en los tiempos establecidos.</p>

    <p>Autorizo al personal del colegio, asegurar y proporcionar primeros auxilios a mi hijo(a) en caso de accidente e informarme a la brevedad posible al teléfono </p>

    <p>Persona autorizada para recoger al alumno(a) <input type=\"textfield\" name=\"persona-autorizada\"></p>

    <p>Persona alterna para recoger al alumno(a) <input type=\"textfield\" name=\"persona-alterna\"></p>

    <p>Cuenta con seguro médico el alumno(a): Si <input type=\"radio\" name=\"seguro-medico\"> No <input type=\"radio\" name=\"seguro-medico\">     ¿Cuál? <input type=\"textfield\" name=\"seguro-nombre\"></p>

    <p>El alumno(a) recibe algún tratamiento médico en forma habitual: Si <input type=\"radio\" name=\"tratamiento-medico\"> No <input type=\"radio\" name=\"tratamiento-medico\"><br>
    ¿Cuál? <input type=\"textfield\" name=\"tratamiento-nombre\"></p>

    <p>El alumno es alérgico a: <input type=\"textfield\" name=\"alergia\"></p>

    <label><input type=\"date\" name=\"fecha-contrato\">
    Fecha</label>

  </div>

\t<div class=\"wrapper-search\">
\t\t<label>Elegir Profesor: </label><input type=\"textfield\" name=\"teacher\" value=\"";
        // line 122
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["s1"]) ? $context["s1"] : null), "message", array()), "html", null, true));
        echo "\">
\t\t<br>
\t\t<label>Elegir Curso: </label><input type=\"textfield\" name=\"course\" ng-model=\"prueba\">
\t</div>
\t<p>Seconds: ";
        // line 126
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar("{{stopwatch}}"));
        echo "</p>

  <table>
    <tr>
      <th>Num</th>
      <th>ID Estudiante</th>
      <th>Nombre</th>
      <th>Curso</th>
      <th>Falta</th>
      <th>Presente</th>
      <th>Retraso</th>
    </tr>
    <tr ng-repeat='friend in pruebaHttp | filter:filter1'>
      <td>";
        // line 139
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar("{{getCounterValue()}}"));
        echo "</td>
      <td>";
        // line 140
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar("{{friend.nid}}"));
        echo "</td>
      <td>";
        // line 141
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar("{{friend.Nombre}}"));
        echo "</td>
      <td>";
        // line 142
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar("{{friend.field_c}}"));
        echo "</td>
      <td><input type=\"radio\" name=\"assitance-";
        // line 143
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar("{{friend.nid}}"));
        echo "\"></td>
      <td><input type=\"radio\" name=\"assitance-";
        // line 144
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar("{{friend.nid}}"));
        echo "\"></td>
      <td><input type=\"radio\" name=\"assitance-";
        // line 145
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar("{{friend.nid}}"));
        echo "\"></td>
    </tr>
  </table>
</div>";
    }

    public function getTemplateName()
    {
        return "modules/custom/inscripcion/templates/inscripcion.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  205 => 145,  201 => 144,  197 => 143,  193 => 142,  189 => 141,  185 => 140,  181 => 139,  165 => 126,  158 => 122,  96 => 62,  46 => 13,  43 => 11,);
    }
}
/* {#*/
/* /***/
/*  * @file*/
/*  * Default theme implementation to print Angular DATA.*/
/*  **/
/*  **/
/*  * @ingroup themeable*/
/*  *//* */
/*  * */
/* #}*/
/* <div id="content-form-registration" ng-controller="inscripcion">*/
/*   {# Personal Student's Information #}*/
/*   <div class="wrapper-personal-registration">*/
/*     <label>I Información personal del estudiante</label>*/
/*     <br>*/
/*     <div class="wrapper-name-lastname">*/
/*       <label>Apellidos (paterno y materno) </label><input  type="textfied" name="apellidos" placeholder="">*/
/*       <label>Nombres </label><input  type="textfied" name="nombre" placeholder="">  */
/*     </div>*/
/*     <br>*/
/*     <div class="wrapper-dates">*/
/*       <label>Fecha de nacimiento </label><input  type="textfied" name="fechaNac" placeholder="">*/
/*       <label>Lugar de nacimiento </label><input  type="textfied" name="lugarNac" placeholder="">*/
/*       <label>Nacionalidad </label><input  type="textfied" name="nacionalidad" placeholder="">*/
/*     </div>*/
/*     <br>*/
/*     <div class="wrapper-age">*/
/*       <label>Grado al que aplica </label><input  type="textfied" name="curso" placeholder="">*/
/*       <label>Edad al 30 de junio de 2016 </label><input  type="number" name="edad" placeholder="">*/
/*     </div>*/
/*     <br>*/
/*     <div class="wrapper-phones">*/
/*       <label>Cuanto tiempo reside en Cbba </label><input  type="textfied" name="tiempoRes" placeholder="">*/
/*       <label>Teléfono(s) domicilio </label><input  type="textfied" name="telefono" placeholder="">*/
/*       <label>Teléfono(s) celular </label><input  type="textfied" name="celular" placeholder="">*/
/*     </div>*/
/*     <br>*/
/*     <div class="wrapper-address">*/
/*       <label>Dirección: Calle/Av. Nro. Zona</label><input type="textfield" name="direccion" placeholder="">*/
/*     </div>*/
/*     <br>*/
/*     <div class="wrapper-colleges">*/
/*       <label>Enumere en orden cronológico los colegios en el que el estudiante estuvo anteriormente</label>*/
/*       <br>*/
/*       <label>1) Nombre <input type="textfield" name="primer-colegio"> Año <input type="textfield" name="primer-año"> Ciudad/País <input type="textfield" name="primer-pais"></label>*/
/*       <label>2) Nombre <input type="textfield" name="segundo-colegio"> Año <input type="textfield" name="segundo-año"> Ciudad/País <input type="textfield" name="segundo-pais"></label>*/
/*       <label>3) Nombre <input type="textfield" name="tercer-colegio"> Año <input type="textfield" name="tercer-año"> Ciudad/País <input type="textfield" name="tercer-pais"></label>*/
/*     </div>*/
/*     <br>*/
/*     <div class="wrapper-others">*/
/*       <label>Dominio del Ingles: <label><input type="radio" name="idioma" value=""> Nada </label><label><input type="radio" name="idioma" value=""> Poco </label><label><input type="radio" name="idioma" value=""> Fluido </label></label>*/
/*       <label>Otros idiomas: <input type="textfield" name="otros-idiomas" placeholder="Separe los idiomas por comas"></label>*/
/*       <label>Estado de salud del alumno: <input type="textfield" name="estado-salud" placeholder=""></label>*/
/*       <label>¿Fue suspendido o expulsado de algún colegio? <label><input type="radio" name="suspendido" value=""> Si </label><label><input type="radio" name="suspendido" value=""> No </label></label>*/
/*       <label>Si la respuesta es afirmativa, favor indique el motivo</label>*/
/*       <input type="textarea" name="motivo">*/
/*     </div>*/
/*   </div>*/
/*   <br>*/
/* */
/*   {# Personal Family's Information #}*/
/*   <div class="wrapper-personal-family">*/
/*     <label>II Información Personal de la Familia</label>*/
/*     <br>*/
/*     <label>¿Algún miembro se congrega? <label> Si <input type="radio" name="suspendido" value=""></label><label> No <input type="radio" name="suspendido" value=""></label></label>*/
/*     <label>Si su respuesta es afirmativa, favor indique la congregación </label>*/
/*     <input type="textarea" name="motivo">*/
/*     <div class="wrapper-father-information" style="width: 50%;">*/
/*       <label>Nombre completo del padre <input type="textfield" name="nombre-papa"></label>*/
/*       <label>Teléfono casa / Teléfono oficina <input type="textfield" name="telefono-papa"></label>*/
/*       <label>Teléfono Celular / Casilla <input type="textfield" name="celular-papa"></label>*/
/*       <label>Dirección correo electrónico <input type="textfield" name="correo-papa"></label>*/
/*       <label>Dirección domicilio <input type="textfield" name="direccion-papa"></label>*/
/*       <label>Profesión <input type="textfield" name="profesion-papa"></label>*/
/*       <label>Nacionalidad <input type="textfield" name="nacionalidad-papa"></label>*/
/*     </div>*/
/*     <div class="wrapper-mother-information" style="width: 50%;">*/
/*       <label>Nombre completo del madre <input type="textfield" name="nombre-madre"></label>*/
/*       <label>Teléfono casa / Teléfono oficina <input type="textfield" name="telefono-madre"></label>*/
/*       <label>Teléfono Celular / Casilla <input type="textfield" name="celular-madre"></label>*/
/*       <label>Dirección correo electrónico <input type="textfield" name="correo-madre"></label>*/
/*       <label>Dirección domicilio <input type="textfield" name="direccion-madre"></label>*/
/*       <label>Profesión <input type="textfield" name="profesion-madre"></label>*/
/*       <label>Nacionalidad <input type="textfield" name="nacionalidad-madre"></label>*/
/*     </div>*/
/*     <label>Indique los hijos que conforman su familia</label>*/
/*     <table>*/
/*       <tr><th></th><th>Nombre completo</th><th>Fecha de nacimiento</th><th>Colegio</th><th>Curso</th></tr>*/
/*       <tr><td><label>1</label></td><td><input type="textfield" name=""></td><td><input type="textfield" name=""></td><td><input type="textfield" name=""></td><td><input type="textfield" name=""></td></tr>*/
/*       <tr><td><label>2</label></td><td><input type="textfield" name=""></td><td><input type="textfield" name=""></td><td><input type="textfield" name=""></td><td><input type="textfield" name=""></td></tr>*/
/*       <tr><td><label>3</label></td><td><input type="textfield" name=""></td><td><input type="textfield" name=""></td><td><input type="textfield" name=""></td><td><input type="textfield" name=""></td></tr>*/
/*       <tr><td><label>4</label></td><td><input type="textfield" name=""></td><td><input type="textfield" name=""></td><td><input type="textfield" name=""></td><td><input type="textfield" name=""></td></tr>*/
/*       <tr><td><label>5</label></td><td><input type="textfield" name=""></td><td><input type="textfield" name=""></td><td><input type="textfield" name=""></td><td><input type="textfield" name=""></td></tr>*/
/*     </table>*/
/*     <label>Favor indicar si los padres son: <label>Divorciados <input type="radio" name="estado-padre"></label><label>Padre fallecido <input type="radio" name="estado-padre"></label><label>Madre fallecida <input type="radio" name="estado-padre"></label></label>*/
/*   </div>*/
/*   <br>*/
/*   <div class="wrapper-contract">*/
/*     <p>Estoy en conocimiento que la aceptación de mi(s) hijos(as) como alumno(a) del colegio Cristo Nación esta sujeta a condiciones estipuladas en el Estatuto Orgánico y Reglamento Interno.</p>*/
/* */
/*     <p>Entiendo que al momento de ser admitido mi hijo(a), cancelaré la primera cuota y todos los demás conceptos relacionados al costo de la colegiatura mensual y en los tiempos establecidos.</p>*/
/* */
/*     <p>Autorizo al personal del colegio, asegurar y proporcionar primeros auxilios a mi hijo(a) en caso de accidente e informarme a la brevedad posible al teléfono </p>*/
/* */
/*     <p>Persona autorizada para recoger al alumno(a) <input type="textfield" name="persona-autorizada"></p>*/
/* */
/*     <p>Persona alterna para recoger al alumno(a) <input type="textfield" name="persona-alterna"></p>*/
/* */
/*     <p>Cuenta con seguro médico el alumno(a): Si <input type="radio" name="seguro-medico"> No <input type="radio" name="seguro-medico">     ¿Cuál? <input type="textfield" name="seguro-nombre"></p>*/
/* */
/*     <p>El alumno(a) recibe algún tratamiento médico en forma habitual: Si <input type="radio" name="tratamiento-medico"> No <input type="radio" name="tratamiento-medico"><br>*/
/*     ¿Cuál? <input type="textfield" name="tratamiento-nombre"></p>*/
/* */
/*     <p>El alumno es alérgico a: <input type="textfield" name="alergia"></p>*/
/* */
/*     <label><input type="date" name="fecha-contrato">*/
/*     Fecha</label>*/
/* */
/*   </div>*/
/* */
/* 	<div class="wrapper-search">*/
/* 		<label>Elegir Profesor: </label><input type="textfield" name="teacher" value="{{s1.message}}">*/
/* 		<br>*/
/* 		<label>Elegir Curso: </label><input type="textfield" name="course" ng-model="prueba">*/
/* 	</div>*/
/* 	<p>Seconds: {{ '{{stopwatch}}' }}</p>*/
/* */
/*   <table>*/
/*     <tr>*/
/*       <th>Num</th>*/
/*       <th>ID Estudiante</th>*/
/*       <th>Nombre</th>*/
/*       <th>Curso</th>*/
/*       <th>Falta</th>*/
/*       <th>Presente</th>*/
/*       <th>Retraso</th>*/
/*     </tr>*/
/*     <tr ng-repeat='friend in pruebaHttp | filter:filter1'>*/
/*       <td>{{'{{getCounterValue()}}'}}</td>*/
/*       <td>{{'{{friend.nid}}'}}</td>*/
/*       <td>{{'{{friend.Nombre}}'}}</td>*/
/*       <td>{{'{{friend.field_c}}'}}</td>*/
/*       <td><input type="radio" name="assitance-{{'{{friend.nid}}'}}"></td>*/
/*       <td><input type="radio" name="assitance-{{'{{friend.nid}}'}}"></td>*/
/*       <td><input type="radio" name="assitance-{{'{{friend.nid}}'}}"></td>*/
/*     </tr>*/
/*   </table>*/
/* </div>*/
