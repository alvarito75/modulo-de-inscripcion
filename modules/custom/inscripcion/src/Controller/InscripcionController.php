<?php

namespace Drupal\inscripcion\Controller;

use Drupal\Core\Controller\ControllerBase;

class InscripcionController extends ControllerBase {

  public function content() {
    // Here the module attachs 'angulars' that is defined in 'inscripcion.libraries.yml'
    $output['#attached']['library'][] = 'inscripcion/inscripcion';
    
    // Theme function
    $output['#theme'] = 'inscripcion';

    return $output;
  }
}