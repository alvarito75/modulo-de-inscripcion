/**
 * @file
 *  Related JavaScript.
 */

// Declare an app.
var registration = angular.module('registration', []);

/**
 * Declare the controller that adds the default value to scope var.
 */
registration.controller('inscripcion', ['$scope', '$http', mollo]);

function mollo($scope, $http) {
  $scope.s1 = {message:'Alvaro'};
  $scope.number = 0;

  $scope.friends = ['Leonilda', 'Noel', 'Kevin', 'Sara', 'Jose'];
  $scope.people = [{firstName: 'Jose', lastName: 'Mamani', age: '26', phone: '72738839'},{firstName: 'Alvaro', lastName: 'Mamani', age: '52', phone: '72738840'}];

  $scope.url = "http://localhost/prueba8/estudiantes-datos";

  var startTime = new Date();
	$scope.stopwatch = 0;

  $http.get($scope.url).success(function(respuesta){
  	if(respuesta){
  		console.log("There is response");
  		console.log($scope.url);
  		$scope.pruebaHttp = respuesta;
			$scope.stopwatch = (new Date() - startTime) / 1000;
			console.log($scope.stopwatch);
  	} else {
      console.log("There is no response");
    }
	});

  $scope.getCounterValue = function(){
    $scope.number++;
    return $scope.number;
  }
};

/**
 * We need to bootstrap the app manually to the container by id, since we have
 *  more than one app on the same page.
 */
angular.element(document).ready(function() {
                                            //ID            APP
  angular.bootstrap(document.getElementById("content-form-registration"),['registration']);
  console.log("There is no anything");
});